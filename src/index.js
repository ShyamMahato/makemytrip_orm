const userModel = require('./models/user');
const locationModel = require('./models/location');
const airlineModel = require('./models/airline');
const airportListModel = require('./models/airportList');
const flightsModel = require('./models/flights');
const flightClassModel = require('./models/flightClass');
const flightDepartureDetailsModel = require('./models/flightDepartureDetails');
const flightPriceDetailsModel = require('./models/flightPriceDetails');
const bookingModel = require('./models/booking');
const travellerDetailsModel = require('./models/travellerDetails');

const createTables = async () => {
	try {
		await userModel().sync();
		await locationModel().sync();
		await airlineModel().sync();
		await airportListModel().sync();
		await flightsModel().sync();
		await flightClassModel().sync();
		await flightDepartureDetailsModel().sync();
		await flightPriceDetailsModel().sync();
		await bookingModel().sync();
		await travellerDetailsModel().sync();

		console.log('Tables created.');
	} catch (error) {
		console.log(error);
	}
};

createTables();
