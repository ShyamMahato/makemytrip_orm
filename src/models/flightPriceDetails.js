const Sequelize = require('sequelize');
const connection = require('./connection');

const FlightPriceDetails = () => {
	return connection.define('flight_price_details', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		flight_departure_details_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'flight_departure_details',
				key: 'id',
			},
			allowNull: false,
		},
		class_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'flight_class',
				key: 'id',
			},
			allowNull: false,
		},
		price: {
			type: Sequelize.TIME,
			allowNull: false,
		},
	});
};

module.exports = FlightPriceDetails;
