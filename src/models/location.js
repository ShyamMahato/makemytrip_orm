const Sequelize = require('sequelize');
const connection = require('./connection');

const Location = () => {
	return connection.define('location', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		country: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		state: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		city: {
			type: Sequelize.STRING,
			allowNull: false,
		},
	});
};

module.exports = Location;
