const Sequelize = require('sequelize');
const connection = require('./connection');

const User = () => {
	return connection.define('users', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		full_name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		phone: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		password: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		verify_email: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});
};

module.exports = User;
