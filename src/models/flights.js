const Sequelize = require('sequelize');
const connection = require('./connection');

const Flights = () => {
	return connection.define('flights', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		airport_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'airport_list',
				key: 'id',
			},
			allowNull: false,
		},
		flight_code: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		from_location_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'location',
				key: 'id',
			},
			allowNull: false,
		},
		to_location_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'location',
				key: 'id',
			},
			allowNull: false,
		},
	});
};

module.exports = Flights;
