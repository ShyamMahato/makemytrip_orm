const Sequelize = require('sequelize');
const connection = require('./connection');

const AirportList = () => {
	return connection.define('airport_list', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		location_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'location',
				key: 'id',
			},
			allowNull: false,
		},
		airport_name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		airport_code: {
			type: Sequelize.STRING,
			allowNull: false,
		},
	});
};

module.exports = AirportList;
