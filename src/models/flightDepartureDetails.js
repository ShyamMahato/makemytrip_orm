const Sequelize = require('sequelize');
const connection = require('./connection');

const FlightDepartureDetails = () => {
	return connection.define('flight_departure_details', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		flight_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'airport_list',
				key: 'id',
			},
			allowNull: false,
		},
		departure_date: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		departure_time: {
			type: Sequelize.TIME,
			allowNull: false,
		},
		arrival_date: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		arrival_time: {
			type: Sequelize.TIME,
			allowNull: false,
		},
		capacity: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		luggage_capacity: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});
};

module.exports = FlightDepartureDetails;
