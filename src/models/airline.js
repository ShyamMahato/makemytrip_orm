const Sequelize = require('sequelize');
const connection = require('./connection');

const Airline = () => {
	return connection.define('airline', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
	});
};

module.exports = Airline;
